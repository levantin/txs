====
txs
====

**txs** is a free and open-source python package for the analysis
of time-resolved x-ray scattering/spectroscopy data. 


GETTING txs
------------

You can run txs on all major platforms. For download and installation:

.. code-block:: bash

    pip install git+https://gitlab.esrf.fr/levantin/txs.git

For installing a specific version (e.g. version 0.1.0):

.. code-block:: bash

    pip install git+https://gitlab.esrf.fr/levantin/txs.git@0.1.0

For upgrading txs to the last version/commit:

.. code-block:: bash

    pip install --upgrade git+https://gitlab.esrf.fr/levantin/txs.git

For forcing the reinstallation of all packages (including dependencies):

.. code-block:: bash

    pip install --force-reinstall git+https://gitlab.esrf.fr/levantin/txs.git


DOCUMENTATION
-------------

* Homepage: https://levantin.gitlab-pages.esrf.fr/txs
* Quick guide: https://confluence.esrf.fr/display/ID09KB/txs+Python+package


SOURCE CODE
-----------

* Source code: https://gitlab.esrf.fr/levantin/txs


LICENSE
-------

txs is available under MIT License. See LICENSE.rst for more details.

