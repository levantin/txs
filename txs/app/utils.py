from __future__ import annotations

from collections.abc import Iterable
from pathlib import Path
from typing import Any

from packaging.version import Version
import numpy as np
from silx.gui import qt


# Numpy v1/v2 array's copy argument compatibility
NP_OPTIONAL_COPY = False if np.version.version.startswith("1.") else None


Q_LABEL = "q (Å⁻¹)"
I_LABEL = "Normalized scattered intensity (a.u.)"


def titleFromResults(results: dict) -> str:
    """Returns a title "dataset/scan" from datared result dict"""
    return "/".join(
        part
        for part in Path(results["azav"]["folder"]).absolute().parts[-2:]
        if part != "/"
    )


class WheelEventFilter(qt.QObject):
    """QObject that filters-out wheel events of registered widgets when they don't have focus"""

    def eventFilter(self, obj: qt.QObject, event: qt.QEvent) -> bool:
        """Event filter that filters-out wheel event for widget without focus"""
        if event.type() == qt.QEvent.Wheel:
            return not obj.hasFocus()
        return False

    def filter(self, widgets: Iterable[qt.QWidget]):
        """Install this event filter on the given widgets and disable focus on wheel events"""
        for widget in widgets:
            if widget.focusPolicy() == qt.Qt.FocusPolicy.WheelFocus:
                widget.setFocusPolicy(qt.Qt.FocusPolicy.StrongFocus)
            widget.installEventFilter(self)


class FileCompleter(qt.QCompleter):
    """File path auto-completion"""

    def __init__(self, parent: qt.Qobject | None = None):
        super().__init__(parent)
        model = qt.QFileSystemModel(self)
        if Version(qt.qVersion()) >= Version("5.14"):
            model.setOption(qt.QFileSystemModel.DontWatchForChanges, True)
        model.setRootPath("/")

        self.setModel(model)
        self.setCompletionRole(qt.QFileSystemModel.FileNameRole)


class Property:
    """Wrapper to get/set given QObject's property.

    Access the 'user' property if no name is provided.
    """

    def __init__(self, qobject: qt.QObject, name: str | None = None) -> None:
        self.__qobject = qobject

        metaObject = self.__qobject.metaObject()
        if name is None:
            userProperty = metaObject.userProperty()
            if not userProperty.isValid():
                raise ValueError("No user property")
            name = userProperty.name()

        index = metaObject.indexOfProperty(name)
        if index == -1:
            raise ValueError(f"Object has no property named {name}")
        if not metaObject.property(index).isValid():
            raise ValueError("Property is not valid")

        self.__name = name

    def value(self) -> Any:
        return self.__qobject.property(self.__name)

    def setValue(self, value: Any) -> bool:
        return self.__qobject.setProperty(self.__name, value)


def getNamedChildrenUserProperties(widget: qt.QWidget) -> dict[str, Property]:
    """Returns a mapping of widget name: Widget user property wrapper"""
    userProperties = {}
    for child in widget.findChildren(qt.QWidget):
        name = child.objectName()
        if not name:
            continue
        if name.startswith("qt_"):
            continue  # Skip names used by Qt
        if isinstance(child, qt.QAbstractButton) and not child.isCheckable():
            continue  # Skip buttons with no state
        try:
            userProperties[name] = Property(child)
        except ValueError:
            continue  # Widget has no user property
    return userProperties
