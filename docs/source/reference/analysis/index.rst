analysis module
===============

.. toctree::
    :maxdepth: 1

    svd
    plot
    utils
